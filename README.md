# kind-cluster 
## Describe
Personnal configuration for kind in my environment

## modules
- registry docker in kind-registry:5000
- nginx ingress controller : localhost 80/443
- Istio gateway on load balancer : 172.19.255.200:80
- default stack of http (ingress/service/deployment/Istio gateway/Itio virtual Service)

## Configuration
- 1 master 
- 3 worker nodes

## install
```
kind-with-registry.sh
```

## clean
```
kind delete cluster
docker ps -a --filter name=registry -q | xargs -r docker stop
docker ps -a --filter name=registry -q | xargs -r docker rm -v
```

# test 
```
kubectl get pods
curl http://localhost
curl http://172.19.255.200/
```
