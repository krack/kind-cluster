#!/bin/sh

DIR="$(dirname "$(readlink -f "$0")")"
#install kind if not exist
command -v kind 1> /dev/null
if [ $? -ne 0 ]
then
  curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64
  chmod +x ./kind
  mv ./kind /usr/local/bin/kind
fi
#install istioctl if not exst
command -v istioctl 1> /dev/null
if [ $? -ne 0 ]
then
  curl -L https://istio.io/downloadIstio | sh -
  cd ./istio-*
  chmod +x ./bin/istioctl
  sudo mv ./bin/istioctl /usr/local/bin/istioctl
  cd $DIR
  rm -rf ./istio-*
fi


set -o errexit

# create registry container unless it already exists
reg_name='kind-registry'
reg_port='5000'
running="$(docker inspect -f '{{.State.Running}}' "${reg_name}" 2>/dev/null || true)"

if [ "${running}" != 'true' ]; then
  docker run \
    -d --restart=always -p "${reg_port}:5000" --name "${reg_name}" \
    registry:2
fi

# create a cluster with the local registry enabled in containerd
cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:${reg_port}"]
    endpoint = ["http://${reg_name}:${reg_port}"]
kubeadmConfigPatches:
- |
  apiVersion: kubeadm.k8s.io/v1beta2
  kind: ClusterConfiguration
  metadata:
    name: config
  apiServer:
    extraArgs:
      "enable-admission-plugins": "NamespaceLifecycle,LimitRanger,ServiceAccount,TaintNodesByCondition,Priority,DefaultTolerationSeconds,DefaultStorageClass,PersistentVolumeClaimResize,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota"
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"    
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP 
- role: worker
- role: worker
- role: worker
EOF

# connect the registry to the cluster network
# (the network may already be connected)
docker network connect "kind" "${reg_name}" || true




# Document the local registry
# https://github.com/kubernetes/enhancements/tree/master/keps/sig-cluster-lifecycle/generic/1755-communicating-a-local-registry
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: local-registry-hosting
  namespace: kube-public
data:
  localRegistryHosting.v1: |
    host: "localhost:${reg_port}"
    help: "https://kind.sigs.k8s.io/docs/user/local-registry/"
EOF


# Deploy the nginx Ingress controller
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml

#wait nginx ready
kubectl -n ingress-nginx wait --for=condition=available --all deployments --timeout=-1s
sleep 5s
kubectl -n ingress-nginx wait --for=condition=Ready pods -l app.kubernetes.io/component=controller --timeout=-1s

#install istio
istioctl install --set profile=demo -y
# configure default namesapce
kubectl label namespace default istio-injection=enabled
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.20/samples/bookinfo/platform/kube/bookinfo.yaml
#kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)" 
#kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/metallb.yaml
#kubectl apply -f https://kind.sigs.k8s.io/examples/loadbalancer/metallb-configmap.yaml


#default deployment
kubectl apply -f $DIR/defaultApp.yaml

echo "curl http://localhost"
echo "curl http://172.19.255.200/"